package com.micro.fast.upms.controller;

import com.micro.fast.boot.starter.common.response.BaseConst;
import com.micro.fast.boot.starter.common.response.ServerResponse;
import com.micro.fast.upms.pojo.UpmsRolePermission;
import com.micro.fast.upms.service.UpmsRolePermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
*
* @author lsy
*/
@Api("upmsRolePermission")
@RestController
@RequestMapping("/upmsRolePermission")
public class UpmsRolePermissionController {

  @Autowired
  private UpmsRolePermissionService upmsRolePermissionService;

}
